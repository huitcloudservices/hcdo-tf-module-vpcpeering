# AWS VPC Peering Terraform Module

## Module Overview

This module sets up a VPC Peering endpoint and updates the local route table for the remote end's CIDR

## Module Invocation/Usage

```
module "vpc_peering_to_account" {
    source          = "bitbucket.org/huitcloudservices/hcdo-tf-module-vpcpeering"
    peer_owner_id   = "123456789012"
    peer_vpc_id     = "vpc-1234abc"
    peer_cidr       = "10.2.0.0/20"
    vpc_id          = "vpc-abc1234"
    route_table_id  = "rtb-1234abcd"   
}
```

## Module Parameters

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| peer_owner_id | AWS Account ID for Remote Peer | - | yes |
| peer_vpc_id | AWS VPC ID for Remote Peer | - | yes |
| vpc_id | AWS VPC ID for Local Peer | - | yes |
| route_table_id | AWS Route Table ID for Local Peer | - | yes |
| peer_cidr | CIDR for Remote Peer to add to route table | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| peer_connection_id | ID of the VPC Peering Connection |
| peer_connection_status | Accept Status of the VPC Peering connection on the remote end |