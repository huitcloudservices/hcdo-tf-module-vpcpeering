# All variables should have type defined to prevent inference of type

variable "peer_owner_id" {
  type = "string"
  description = "AWS Account ID for Remote Peer"
}

variable "peer_vpc_id" {
  type = "string"
  description = "AWS VPC ID for Remote Peer"
}

variable "vpc_id" {
  type = "string"
  description = "AWS VPC ID for Local Peer"
}

variable "route_table_id" {
  type = "string"
  description = "AWS Route Table ID for Local Peer"
}

variable "peer_cidr" {
  type = "string"
  description = "CIDR for Remote Peer to add to route table"
}