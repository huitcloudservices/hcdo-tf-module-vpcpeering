# This file should contain all outputs from the module

output "peer_connection_id" { value = "${aws_vpc_peering_connection.vpc_peering.id}" }
output "peer_connection_status" { value = "${aws_vpc_peering_connection.vpc_peering.accept_status}" }