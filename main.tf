resource "aws_vpc_peering_connection" "vpc_peering" {
    peer_owner_id = "${var.peer_owner_id}"
    peer_vpc_id = "${var.peer_vpc_id}"
    vpc_id = "${var.vpc_id}"
}

resource "aws_route" "peering_route" {
  route_table_id            = "${var.route_table_id}"
  destination_cidr_block    = "${var.peer_cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.vpc_peering.id}"
}